<?php
/**
 * @file
 * Administrative page callbacks for the litelog module.
 */

/**
 * Admin settings form callback.
 *
 * @see litelog_menu()
 */
function litelog_admin_settings() {
  $form = array();

  $options_disallowed_types = array_merge(
    drupal_map_assoc(array_keys(variable_get('litelog_disallowed_types'))),
    litelog_default_types()
  );
  asort($options_disallowed_types);

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Types'),
    '#description' => t('The list below is dynamically generated from current content of <em>watchdog</em> database table.'),
    '#collapsible' => TRUE,
  );
  $form['types']['litelog_disallowed_types'] = array(
    '#title' => t('Disallowed types'),
    '#description' => t('Select types which are not allowed to be logged.'),
    '#type' => 'checkboxes',
    '#options' => $options_disallowed_types,
    '#default_value' => variable_get('litelog_disallowed_types', array()),
  );
  $form['types']['litelog_disallowed_types_custom'] = array(
    '#title' => t('Custom disallowed types'),
    '#description' => t('Enter additional custom disallowed types (one type per line).'),
    '#type' => 'textarea',
    '#default_value' => variable_get('litelog_disallowed_types_custom'),
  );

  $form['severities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Severity levels'),
    '#collapsible' => TRUE,
  );
  $form['severities']['litelog_disallowed_severities'] = array(
    '#title' => t('Disallowed severities'),
    '#description' => t('Select severity levels which are not allowed to be logged.'),
    '#type' => 'checkboxes',
    '#options' => litelog_severity_level_options(),
    '#default_value' => variable_get('litelog_disallowed_severities', array()),
  );

  $form['messages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Messages'),
    '#collapsible' => TRUE,
  );
  $form['messages']['litelog_disallowed_messages'] = array(
    '#title' => t('Disallowed messages'),
    '#description' => t('Enter patterns for messages which are not allowed to be logged (one pattern per line). Use <a href="@url">preg_match</a> pattern format, but without leading and trailing slashes (they will be added automatically). This matches both watchdog raw messages and translated messages alike. Ex.<br><code>@type: added %title.</code><br>generally silences addition of all content types, but<br><code>^page: added</code><br>silences only page content-type creation messages.', array(
      '@url' => url('http://php.net/manual/en/function.preg-match.php'),
    )),
    '#type' => 'textarea',
    '#default_value' => variable_get('litelog_disallowed_messages'),
  );

  return system_settings_form($form);
}

/**
 * Provides an options array of severity levels with custom keys.
 *
 * The issue with using native watchdog_severity_levels() as options array
 * is that 'emergency' level has key value of 0, resulting in array element
 * always as '0 => 0', regardless of whether it was checked or not, so we
 * wouldn't know its real state.
 *
 * @return array
 *   An array of severity levels with custom keys.
 */
function litelog_severity_level_options() {
  $levels = watchdog_severity_levels();
  $options = array();
  foreach ($levels as $level => $name) {
    $options['s' . $level] = $name;
  }
  return $options;
}
