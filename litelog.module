<?php

/**
 * @file
 * Provides core functionality of Litelog.
 */

/**
 * Implements hook_menu().
 */
function litelog_menu() {
  $items = array();

  // Default local task for core dblog settings.
  $items['admin/config/development/logging/default'] = array(
    'title' => 'Core',
    'description' => 'Default drupal settings.',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/development/logging/litelog'] = array(
    'title' => 'Litelog',
    'description' => 'Configure what can be logged in watchdog table.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('litelog_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'litelog.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_module_implements_alter().
 */
function litelog_module_implements_alter(&$implementations, $hook) {
  // Unsets dblog's implementation of hook_watchdog() - we will use this
  // module's litelog_watchdog() instead.
  if ($hook == 'watchdog' && isset($implementations['dblog'])) {
    unset($implementations['dblog']);
  }
}

/**
 * Implements hook_watchdog().
 *
 * @see dblog_watchdog()
 */
function litelog_watchdog(array $log_entry) {
  // Make sure this log type is on the module's type list.
  $types = variable_get('litelog_types_list', litelog_default_types());
  if (!empty($log_entry['type']) && !isset($types[$log_entry['type']])) {
    $types[$log_entry['type']] = $log_entry['type'];
    variable_set('litelog_types_list', $types);
  }

  // Do not log if event severity is not in the list of allowed severities.
  // We have custom values in 'litelog_disallowed_severities' variable,
  // hence the need to add that 's' at the beginning.
  // See litelog_severity_level_options() for more info.
  if (isset($log_entry['severity']) && in_array('s' . $log_entry['severity'], variable_get('litelog_disallowed_severities', array()), TRUE)) {
    return;
  }

  if (!empty($log_entry['type'])) {
    // Do not log if entry type is on the list of disallowed types.
    if (in_array($log_entry['type'], variable_get('litelog_disallowed_types', array()), TRUE)) {
      return;
    }

    // Do not log if entry type is on the list of custom disallowed types.
    if ($disallowed_types_custom = variable_get('litelog_disallowed_types_custom')) {
      $custom_types = explode("\n", $disallowed_types_custom);
      if (is_array($custom_types) && in_array($log_entry['type'], $custom_types, TRUE)) {
        return;
      }
    }
  }

  // Do not log if entry message matches one of disallowed message patterns.
  if (!empty($log_entry['message']) && ($disallowed_patterns = variable_get('litelog_disallowed_messages'))) {
    $log_entry_variables = isset($log_entry['variables']) ? $log_entry['variables'] : array();
    $patterns = explode("\n", $disallowed_patterns);
    if (is_array($patterns)) {
      foreach ($patterns as $pattern) {
        if (preg_match('/' . trim($pattern) . '/', $log_entry['message']) ||
            preg_match('/' . trim($pattern) . '/', t($log_entry['message'], $log_entry_variables))) {
          return;
        }
      }
    }
  }

  Database::getConnection('default', 'default')->insert('watchdog')
    ->fields(array(
      'uid' => $log_entry['uid'],
      'type' => substr($log_entry['type'], 0, 64),
      'message' => $log_entry['message'],
      'variables' => serialize($log_entry['variables']),
      'severity' => $log_entry['severity'],
      'link' => substr($log_entry['link'], 0, 255),
      'location' => $log_entry['request_uri'],
      'referer' => $log_entry['referer'],
      'hostname' => substr($log_entry['ip'], 0, 128),
      'timestamp' => $log_entry['timestamp'],
    ))
    ->execute();
}

/**
 * Provides an array of event list from current watchdog table events.
 *
 * @return array
 *   An array of event list from current watchdog table events.
 *
 * @see litelog_watchdog()
 * @see litelog_admin_settings()
 */
function litelog_default_types() {
  $result = db_select('watchdog', 'w')
    ->distinct()
    ->fields('w', array('type'))
    ->execute()
    ->fetchCol();
  return drupal_map_assoc($result);
}
